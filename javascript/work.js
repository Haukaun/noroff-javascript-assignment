import state, { updateFields } from "./state.js";

const bankButton = document.querySelector(".bank-button");
const workButton = document.querySelector(".work-button");
const repayButton = document.querySelector(".repay-button");

/**
 * Transfers the earned pay to the bank balance.
 * If there's an outstanding loan, 10% is deducted from the pay to repay the loan.
 */
function transferToBank() {
  state.balance += state.pay;
  state.pay = 0;

  let deduction = 0;

  if (state.loan > 0) {
    deduction = state.pay * 0.1;

    const actualDeduction = Math.min(deduction, state.loan);

    state.loan -= actualDeduction;
    state.pay -= actualDeduction;

    updateFields();
  }

  state.balance += state.pay;

  state.pay = 0;

  updateFields();
}

/**
 * Repays the loan either using the earned pay or the bank balance, based on user's choice.
 */
function repayLoan() {
  if (state.pay === 0) {
    let response = window.confirm(
      "You must work first to get money to repay your loan." +
        " Do you want to repay loan from your bank balance?"
    );

    if (response) {
      if (state.balance >= state.loan) {
        state.balance -= state.loan;
        state.loan = 0;
      } else {
        state.loan -= state.balance;
        state.balance = 0;
      }
      updateFields();
    }
  }

  if (state.pay >= state.loan) {
    state.pay -= state.loan;
    state.loan = 0;

    state.balance += state.pay;
    state.pay = 0;
  } else {
    state.loan -= state.pay;
    state.pay = 0;
  }
  updateFields();
}

// Event listeners for transferring money to bank and repaying loan.
bankButton.addEventListener("click", transferToBank);
repayButton.addEventListener("click", repayLoan);

/**
 * Adds salary to the current pay when the work button is clicked.
 */
workButton.addEventListener("click", function () {
  state.pay += 100;
  updateFields();
});
