//State.js is a file that contains the state of the application.
//It is a single source of truth for the application.
//It is a good practice to keep the state of the application in a single place.
//This makes it easier to debug and maintain the application. It also makes it easier to share the state between different parts of the application.

// The state object contains global the states for the application application.
let state = {
  balance: 0,
  loan: 0,
  pay: 0,
  laptops: [],
  currentLaptop: {},
};
export default state;

export const loanBalance = document.getElementById("loan-balance");
export const payBalance = document.getElementById("pay-balance");
export const bankBalance = document.getElementById("bank-balance");

export const BASE_API_URL = "https://hickory-quilled-actress.glitch.me/";

// The updateFields function is used to update the fields in the application.
export function updateFields() {
  loanBalance.innerText = state.loan;
  payBalance.innerText = state.pay;
  bankBalance.innerText = state.balance;
  if (state.loan <= 0) {
    document.querySelector(".repay-button").style.display = "none";
    document.getElementById("loan-info").style.display = "none";
  }
}
