import state, { BASE_API_URL, updateFields } from "./state.js";

const dropDown = document.getElementById("dropdown");
const featureContainer = document.querySelector(".feature-wrapper");
const laptopImage = document.getElementById("laptop-image");
const laptopHeader = document.getElementById("laptop-header");
const laptopDescription = document.getElementById("laptop-description");
const laptopPrice = document.getElementById("laptop-price");
const buyButton = document.querySelector(".buy-button");

/**
 * Fetches the laptop details from the API and initializes dropdown and laptop details.
 */
function fetchLaptops() {
  fetch(BASE_API_URL + "computers/")
    .then((response) => response.json())
    .then((data) => {
      state.laptops = data;
      for (const element of state.laptops) {
        let newOption = document.createElement("option");
        newOption.value = element.id;
        newOption.text = element.title;
        dropDown.appendChild(newOption);
      }
      state.currentLaptop = state.laptops[0];
      displayLaptop(state.currentLaptop);
      setLaptopFeatures(state.currentLaptop.specs);
    });
}

/**
 * Populates the laptop features in the UI.
 * @param {Array} features - List of laptop features.
 */
function setLaptopFeatures(features) {
  features.forEach((feature) => {
    let newFeature = document.createElement("span");
    newFeature.innerText = "• " + feature;
    featureContainer.append(newFeature);
  });
}

/**
 * Executes the laptop purchase, updates the balance, and shows relevant alerts.
 */
function buyLaptop() {
  if (state.balance >= state.currentLaptop.price) {
    state.balance -= state.currentLaptop.price;
    updateFields();
    alert("Congratz, you got yourself a computer!");
  } else {
    alert("You don't have enough money to buy this computer!");
  }
}
buyButton.addEventListener("click", buyLaptop);

/**
 * Displays the laptop details and image in the UI.
 * @param {Object} laptop - Selected laptop object.
 */
async function displayLaptop(laptop) {
  await fetch(BASE_API_URL + "assets/images/" + laptop.id + ".png").then(
    (data) => {
      if (data.status == 404) {
        laptopImage.src = BASE_API_URL + "assets/images/1.png";
      } else laptopImage.src = data.url;
    }
  );

  laptopHeader.innerText = laptop.title;
  laptopDescription.innerText = laptop.description;
  laptopPrice.innerText = laptop.price;
}

// Event listener for dropdown changes to update laptop display.
dropDown.addEventListener("change", (event) => {
  const laptopId = event.target.value;
  state.currentLaptop = state.laptops.find((laptop) => laptop.id == laptopId);
  featureContainer.innerHTML = "";
  displayLaptop(state.currentLaptop);
  setLaptopFeatures(state.currentLaptop.specs);
});

fetchLaptops();
