import state, { updateFields } from "./state.js";

const loanButton = document.querySelector(".loan-button");

/**
 * The `getLoan` function allows the user to apply for a loan.
 *
 * Constraints:
 * 1. A loan can only be obtained if there's no outstanding loan.
 * 2. The loan amount cannot exceed double the current bank balance.
 *
 */

function getLoan() {
  // Check if there is an outstanding loan
  if (state.loan > 0) {
    alert("You must repay your outstanding loan before taking another.");
    return;
  }

  const loanAmount = parseFloat(prompt("Enter the loan amount:", "0"));

  //Check if the loan amount is a valid number
  if (isNaN(loanAmount)) {
    alert("Please enter a valid number.");
    return;
  }

  // Check if loan is more than double the bank balance
  if (loanAmount > state.balance * 2) {
    alert(
      `You cannot get a loan more than double of your bank balance. Your maximum allowed loan amount is ${
        bankBalance * 2
      }`
    );
    return;
  }

  // Grant the loan
  state.loan = loanAmount;
  state.balance += loanAmount;

  document.getElementById("loan-info").style.display = "block";

  updateFields();

  document.querySelector(".repay-button").style.display = "block";
}

loanButton.addEventListener("click", getLoan);
